# Hello, World
Simple "Hello, World" application intended for demonstrating Node.js on Raspberry Pi

## Installation
```
$ npm install
```

## Run
```
$ node hello.js
Hello, world!
```
