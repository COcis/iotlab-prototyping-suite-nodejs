IoTLAB is an extensible open-source IoT prototyping framework

"One ring to bind them all" J.R.R.Tolkien - LOTR

With it you can:
-Manage GIT courses with groups and users-integrate all best home automation systems.
-Develop multi-language IDEs for the cloud and desktop using state-of-the-art web technologies.
-Growing ecosystem of over 60 available language servers, delivering intelligent editing support for all major programming languages
-Integrate seamlessly with major vocal assistants, web and bot tecnologies.
-Deploy secure APIs in few clicks.
-Modules for opening IA, ML, NLP gateways. 
-Privacy by design for all the core functonalities.
